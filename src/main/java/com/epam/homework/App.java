package com.epam.homework;


/**
 * <h1>second homework</h1>
 *
 * @author Maksym Kuziv
 * @version 1.0
 * @since 2019-11-07
 */

/**
 * My first Maven project written in IntelliJ IDEA.<br>
 * To run main method user have to enter three values;<br>
 * First is starting value of range;<br>
 * Second is ending value of range;<br>
 * Third is quantity of Fibonacci numbers;<br>
 */
public class App {
    /**
     * The number of summ odd digits.
     */
    private int sumOdd = 0;
    /**
     * The number of summ even digits.
     */
    private int sumEven = 0;

    /**
     * This method counts sum for all even numbers in range [start; end]).
     *
     * @param start int number to start with
     * @param end   int number to end with
     * @return odd digits
     */
    public final void numbers(final int start, final int end) {
        int countNumber = 0;
        int count = 0;

        count = end + 1;
        count = count - start;
        System.out.println("all - " + count);
        int[] nums = new int[count];

        for (int i = start; i <= end; i++) {
            nums[countNumber] = i;
            System.out.print(nums[countNumber] + " ");
            countNumber++;
        }
        System.out.println();
        countNumber = 0;
        System.out.print("Even: ");
        for (int i = 0; i <= count - 1; i++) {
            if (nums[i] % 2 == 0) {
                System.out.print(nums[i] + ", ");
                sumOdd += nums[i];
            }
        }
        System.out.println();
        System.out.print("Odd: ");
        for (int i = count - 1; i >= 0; i--) {
            if (nums[i] % 2 != 0) {
                System.out.print(nums[i] + ", ");
                sumEven += nums[i];
            }
        }
        System.out.println();
    }

    /**
     * This method counts sum for all even numbers in methid "Numbers".
     *
     * @return sum of even digits
     */
    public final int getSumEven() {
        return sumEven;
    }

    /**
     * This method counts sum for all even numbers in methid "Numbers".
     *
     * @return sum of odd digits
     */
    public final int getSumOdd() {
        return sumOdd;
    }

    /**
     * The method which find Fibonacci numbers.
     * returns last of them odd
     * returns last of them even
     * calculated percent of odd digits
     * calculated percent of odd digits
     *
     * @param number int number of Fibonacci length
     */
    public final void fibonacci(final int number) {
        int percentOddCount = 0;
        int percentEvenCount = 0;
        long lastEvenDigit = 0;     //largest even;
        long lastOddDigit = 0;      //largest odd;
        double percentOddFibonacci = 0;
        double percentEvenFibonacci = 0;
        final double percentFib = 100;

        int lengthArrayFibonacci = number;

        long[] arrayFib = new long[lengthArrayFibonacci];

        for (int i = 0; i < arrayFib.length; i++) {
            if (i < 2) {
                arrayFib[i] = 1;
            } else {
                arrayFib[i] = arrayFib[i - 1] + arrayFib[i - 2];
            }
        }
        System.out.println("Length of Fibonacci = " + arrayFib.length);
        for (int i = arrayFib.length; i > 1; i--) {
            if (lastEvenDigit == 0) {
                if (arrayFib[i - 1] % 2 == 0) {
                    lastEvenDigit = arrayFib[i - 1];
                }
            }
            if (lastOddDigit == 0) {
                if (arrayFib[i - 1] % 2 != 0) {
                    lastOddDigit = arrayFib[i - 1];
                }
            }
            if (lastEvenDigit != 0 && lastOddDigit != 0) {
                break;
            }
        }
        System.out.println("last is even digit is " + lastEvenDigit);
        System.out.println("last is odd digit is " + lastOddDigit);

        for (int i = 0; i < arrayFib.length; i++) {
            if (arrayFib[i] % 2 == 0) {
                percentEvenCount++;
            } else {
                percentOddCount++;
            }
        }
        percentOddFibonacci = percentFib * percentOddCount / arrayFib.length;
        percentEvenFibonacci = percentFib * percentEvenCount / arrayFib.length;
        System.out.println("percent of odd = " + percentOddFibonacci);
        System.out.println("percent of even = " + percentEvenFibonacci);

    }
    /**
     * This is the entry point of my homework
     *
     * @param args User can enter the interval and quantity
     *            of Fibonacci numbers to generate
     *            so he has to enter THREE arguments, or
     *            default parameters is used
     *            interval: [5; 15]
     *            Fibonacci numbers quantity: 14
     */
    public static void main(String[] args) {
        App app = new App();
        app.numbers(5, 16);
        System.out.println("sum of event = " + app.getSumOdd());
        System.out.println("sum of odd = " + app.getSumEven());
        app.fibonacci(14);

    }
}

